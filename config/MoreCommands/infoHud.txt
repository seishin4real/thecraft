// Have a look at https://github.com/PlanetTeamSpeakk/MoreCommands to see what variables you can use here.
{DF}Player: {SF}{playerName}
{DF}FPS: {SF}{fps}
{DF}X: {SF}{x}
{DF}Y: {SF}{y}
{DF}Z: {SF}{z}
{DF}Pitch: {SF}{pitch}
{DF}Yaw: {SF}{yaw}
{DF}Facing: {SF}{facing}
{DF}Biome: {SF}{biome}
{DF}Speed: {SF}{avgSpeed}
