const fs = require('fs');
const { join } = require('path');


const mods = fs.readdirSync('mods');
const srvMods = fs.readdirSync('mods-srv');
const srvNoChanges = [];
const srvMissing = [];
const newMissing = [];

const srvRemoved = [];
const updateAvailable = [];

for (const m of srvMods) {
  (mods.includes(m) ? srvNoChanges : srvMissing).push(m);
}
for (const m of mods) {
  if (!srvMods.includes(m)) { newMissing.push(m); }
}

for (const m of srvMissing) {
  const splitter = m.includes('-') ? '-' : '_';
  const msegs = m.split(splitter);
  const possibleMatches = mods.filter(mod => mod.startsWith(msegs[0]));
  if (!possibleMatches.length) { srvRemoved.push(m); }
  else if (possibleMatches.length === 1) { updateAvailable.push(possibleMatches[0]); }
  else {
    const deeperFilter = possibleMatches.filter(mod => mod.startsWith(`${msegs[0]}${splitter}${msegs[1]}`));
    if (!deeperFilter.length) { srvRemoved.push(m); }
    else if (deeperFilter.length === 1) { updateAvailable.push(deeperFilter[0]); }
    else {
      console.log('STILL UNRESOLVED: ', m);
    }
  }
}

const newReallyMissing = newMissing.filter(m => !updateAvailable.includes(m));


console.log(`Unchanged: ${srvNoChanges.length}`);
for (const m of srvNoChanges) {
  fs.copyFileSync(
    join('mods-srv', m),
    join('mods-update', m)
  )
}

console.log(`Updated: ${updateAvailable.length}`);
for (const m of updateAvailable) {
  fs.copyFileSync(
    join('mods', m),
    join('mods-update', m)
  )
}

console.log('srvMissing');
console.log(srvMissing);

console.log('srvRemoved');
console.log(srvRemoved);

console.log('updateAvailable');
console.log(updateAvailable);

console.log('newMissing');
console.log(newMissing);

console.log('newReallyMissing');
console.log(newReallyMissing);
